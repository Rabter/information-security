﻿using System;

namespace lab_5_signature
{
    class Program
    {
        static void Main(string[] args)
        {
            Signature sign = new Signature("SHA512");
            string choice;
            do
            {
                Console.WriteLine("Select an option:\n1. Create signature\n2. Verify file");
                choice = Console.ReadLine();
            } while (choice != "1" && choice != "2");
            Console.WriteLine("Enter file name:");
            string file = Console.ReadLine();
            Console.WriteLine("Enter signature file name:");
            string signature = Console.ReadLine();
            if (choice == "1")
            {
                sign.create(file, signature);
                Console.WriteLine("Signature file has been sucsessfully created");
            }
            else
            {
                if(sign.verify(file, signature))
                    Console.WriteLine("File is valid");
                else
                    Console.WriteLine("File has been corrupted");
            }
            Console.Read();
        }
    }
}

﻿using System.Security.Cryptography;
using System.IO;

namespace lab_5_signature
{
    class Signature
    {
        HashAlgorithm hashAlgorithm;

        RSACryptoServiceProvider cryptoProvider;
        RSAPKCS1SignatureDeformatter deformatter;
        RSAPKCS1SignatureFormatter formatter;

        public Signature(string hashAlgorithmName)
        {
            hashAlgorithm = HashAlgorithm.Create(hashAlgorithmName);

            cryptoProvider = new RSACryptoServiceProvider();
            formatter = new RSAPKCS1SignatureFormatter(cryptoProvider);
            deformatter = new RSAPKCS1SignatureDeformatter(cryptoProvider);

            formatter.SetHashAlgorithm(hashAlgorithmName);
            deformatter.SetHashAlgorithm(hashAlgorithmName);
        }

        public void create(string filename, string signatureFilename)
        {
            using (FileStream reader = new FileStream(filename, FileMode.Open))
            {
                using (BinaryWriter writer = new BinaryWriter(new FileStream(signatureFilename, FileMode.Create)))
                {
                    byte[] key = cryptoProvider.ExportCspBlob(false);
                    writer.Write(key.Length);
                    writer.Write(key);

                    byte[] hash = hashAlgorithm.ComputeHash(reader);
                    byte[] signed_hash = formatter.CreateSignature(hash);

                    writer.Write(signed_hash.Length);
                    writer.Write(signed_hash);
                }
            }
        }

        public bool verify(string filename, string signatureFilename)
        {
            bool result;
            using (BinaryReader signature_reader = new BinaryReader(new FileStream(signatureFilename, FileMode.Open)))
            {
                using (FileStream reader = new FileStream(filename, FileMode.Open))
                {
                    byte[] key = signature_reader.ReadBytes(signature_reader.ReadInt32());
                    cryptoProvider.ImportCspBlob(key);

                    byte[] signed_hash = signature_reader.ReadBytes(signature_reader.ReadInt32());
                    byte[] hash = hashAlgorithm.ComputeHash(reader);

                    result = deformatter.VerifySignature(hash, signed_hash);
                }
            }
            return result;
        }
    }
}
#include <algorithm>
#include "reflector.h"

Reflector::Reflector(unsigned amount): Ring(amount)
{
    for (unsigned i = 0; i < amount; ++i)
        slots[i] = i;

    std::vector<unsigned> swapper(slots); // An array of non-repeating indexes
    std::random_shuffle(swapper.begin(), swapper.end()); // in random order

    for (unsigned i = 0; i < amount - 1; i += 2)
        std::swap(slots[swapper[i]], slots[swapper[i + 1]]); // Once a pair is swapped, none of it's member will be swapped again
}

unsigned Reflector::affect(unsigned index) const
{
    return slots[index];
}

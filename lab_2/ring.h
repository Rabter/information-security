#ifndef RING_H
#define RING_H

#include <vector>

class Ring
{
public:
    Ring(unsigned amount): slots(amount) {}
    Ring(const std::vector<unsigned> slots): slots(slots) {}

    const std::vector<unsigned>& all_slots() const { return slots; }
    virtual inline unsigned affect(unsigned index) const = 0;

    virtual ~Ring() {};

protected:
    std::vector<unsigned> slots;
};

#endif // RING_H

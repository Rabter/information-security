#ifndef ROTOR_H
#define ROTOR_H

#include "ring.h"

class Rotor: public Ring
{
public:
    Rotor(unsigned amount);
    Rotor(std::vector<unsigned> slots, unsigned pos): Ring(slots), pos(pos) {}
    bool rotate();

    inline unsigned get_pos() const { return pos; }

    unsigned affect(unsigned index) const override;
    unsigned affect_backwards(unsigned index) const;

    ~Rotor() {}

private:
    unsigned pos;
};

#endif // ROTOR_H

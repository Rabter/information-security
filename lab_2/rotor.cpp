#include <algorithm>
#include <ctime>
#include "rotor.h"

Rotor::Rotor(unsigned amount): Ring(amount)
{
    for (unsigned i = 0; i < amount; ++i)
        slots[i] = i;
    std::random_shuffle(slots.begin(), slots.end());
    pos = rand() % amount;
}

bool Rotor::rotate()
{
    ++pos;
    if (pos == slots.size())
    {
        pos = 0;
        return true;
    }
    return false;
}

unsigned Rotor::affect(unsigned index) const
{
    return slots[(pos + index) % slots.size()];
}

unsigned Rotor::affect_backwards(unsigned index) const
{
    unsigned value;
    bool not_found = true;
    for (unsigned j = 0; not_found; ++j)
    {
        if (slots[j] == index)
        {
            not_found = false;
            value = (j - pos + slots.size()) % slots.size();
        }
    }
    return value;
}

#ifndef REFLECTOR_H
#define REFLECTOR_H

#include "ring.h"

class Reflector: public Ring
{
public:
    Reflector(unsigned amount);
    Reflector(std::vector<unsigned> slots): Ring(slots) {}

    unsigned affect(unsigned index) const override;

    ~Reflector() {}
};

#endif // REFLECTOR_H

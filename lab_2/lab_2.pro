TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        enigma.cpp \
        main.cpp \
        reflector.cpp \
        rotor.cpp

HEADERS += \
    enigma.h \
    reflector.h \
    ring.h \
    rotor.h

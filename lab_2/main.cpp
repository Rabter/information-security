#include <iostream>
#include <fstream>
#include <string>
#include "enigma.h"
#define BYTE_VARIETY 256

void encrypt(Enigma &machine, const std::string &f1, const std::string &f2)
{
    std::ifstream fin(f1, std::ios::binary);
    std::ofstream fout(f2, std::ios::binary);

    char byte;
    fin.read(&byte, 1);
    while (!fin.eof())
    {
        unsigned encryptee = static_cast<unsigned>(static_cast<unsigned char>(byte));
        machine.encrypt(encryptee);
        byte = static_cast<char>(encryptee);
        fout.write(&byte, 1);
        fin.read(&byte, 1);
    }
}

int main()
{
    Enigma machine;

    char choice = 0;
    std::cout << "Would you like to create a random enigma machine? (Y/n):\n";
    std::cin >> choice;
    choice = std::toupper(choice);

    while (choice != 'Y' && choice != 'N')
    {
        std::cout << "Input error. Enter Y or n.\nWould you like to create a random enigma machine? (Y/n):\n";
        std::cin >> choice;
        choice = std::toupper(choice);
    }

    std::string fname, fname2;
    std::cout << "Enter configuration file name:\n";
    std::cin >> fname;
    if (choice == 'Y')
    {
        machine.init_auto(BYTE_VARIETY);
        machine.save(fname);
    }
    else
        machine.init(fname);

    std::cout << "Enter the name of the file to encrypt:\n";
    std::cin >> fname;
    std::cout << "Enter the name of the encrypted file:\n";
    std::cin >> fname2;

    encrypt(machine, fname, fname2);

    std::cout << "Encrypted successfully!\n";

    return 0;
}

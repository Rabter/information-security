#include <fstream>
#include <algorithm>
#include <ctime>
#include "enigma.h"

void Enigma::init_auto(unsigned alphabet_length)
{
    srand(time(NULL));
    this->alphabet_length = alphabet_length;
    reflector = new Reflector(alphabet_length);
    rotors = std::vector<Rotor*>(3);
    for (Rotor *&rotor: rotors)
        rotor = new Rotor(alphabet_length);

    swaps = std::vector<unsigned>(alphabet_length);
    for (unsigned i = 0; i < alphabet_length; ++i)
        swaps[i] = i;

    unsigned numbers_to_swap = rand() % alphabet_length / 2;
    std::vector<unsigned> unswapped(swaps);

    while (numbers_to_swap--)
    {
        unsigned i = rand() % unswapped.size();
        unsigned index1 = unswapped[i];
        unswapped.erase(unswapped.begin() + i);
        i = rand() % unswapped.size();
        unsigned index2 = unswapped[i];
        unswapped.erase(unswapped.begin() + i);

        swaps[index1] = index2;
        swaps[index2] = index1;

    }

    initialized = true;
}

void Enigma::init(std::string cfg_file)
{
    std::ifstream fin(cfg_file);
    unsigned rotors_amount;
    fin >> rotors_amount;
    fin >> alphabet_length;;
    rotors = std::vector<Rotor*>(rotors_amount);

    std::vector<unsigned> slots(alphabet_length);
    for (Rotor *&rotor: rotors)
    {
        unsigned pos;
        fin >> pos;
        for (unsigned &slot: slots)
            fin >> slot;
        rotor = new Rotor(slots, pos);
    }

    for (unsigned &slot: slots)
        fin >> slot;
    reflector = new Reflector(slots);

    swaps = std::vector<unsigned>(alphabet_length);
    for (unsigned &num: swaps)
        fin >> num;

    initialized = true;
}

void Enigma::save(std::string cfg_file)
{
    std::ofstream fout(cfg_file);
    fout << rotors.size() << ' ' << alphabet_length << '\n';

    for (unsigned i = 0; i < rotors.size(); ++i)
    {
        fout << rotors[i]->get_pos() << '\n';
        const std::vector<unsigned> &slots = rotors[i]->all_slots();
        for (unsigned slot: slots)
            fout << slot << ' ';
        fout << '\n';
    }

    const std::vector<unsigned> &slots = reflector->all_slots();
    for (unsigned slot: slots)
        fout << slot << ' ';
    fout << "\n\n";

    for (unsigned num: swaps)
        fout << num << ' ';
}

int Enigma::encrypt(unsigned &encryptee)
{
    if (encryptee > alphabet_length)
        return -1;

    encryptee = swaps[encryptee];

    bool rotate = true;
    for (unsigned i = 0; rotate && i < rotors.size(); ++i)
        rotate = rotors[i]->rotate();

    for (Rotor* rotor: rotors)
        encryptee = rotor->affect(encryptee);
    encryptee = reflector->affect(encryptee);
    for (int i = rotors.size() - 1; i >= 0; --i)
        encryptee = rotors[i]->affect_backwards(encryptee);

    encryptee = swaps[encryptee];

    return 0;
}

Enigma::~Enigma()
{
    for (Rotor *&rotor: rotors)
        delete rotor;
    delete reflector;
}

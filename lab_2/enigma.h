#ifndef ENIGMA_H
#define ENIGMA_H

#include <vector>
#include <string>
#include "rotor.h"
#include "reflector.h"

class Enigma
{
public:
    Enigma() { initialized = false; }

    void init_auto(unsigned alphabet_length);
    void init(std::string cfg_file);
    void save(std::string cfg_file);

    int encrypt(unsigned &original);

    ~Enigma();

private:
    std::vector<Rotor*> rotors;
    Reflector *reflector;
    bool initialized;
    unsigned alphabet_length;
    std::vector<unsigned> swaps;
};

#endif // ENIGMA_H

﻿#include <vector>
#include <string>
#include <fstream>
#include <winsock2.h>
#include <iphlpapi.h>

#define KEY "Rabter"

#pragma comment(lib, "iphlpapi.lib")

PIP_ADAPTER_INFO* debug;

void mac_to_string(char *res, const BYTE* addr)
{
    for (int i = 0; i < 6; ++i)
    {
        sprintf_s(res, 3, "%02x", *addr++);
        res += 2;
        if (i < 5)
            *res++ = '-';
    }
}

int get_mac_addrs(std::vector<std::string> &res)
{
    IP_ADAPTER_INFO AdapterInfo[16];
    DWORD dwBufLen = sizeof(AdapterInfo);

    DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);
    if (dwStatus != ERROR_SUCCESS)
        return GetLastError();

    PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;
    while (pAdapterInfo)
    {
        if (pAdapterInfo->Type == MIB_IF_TYPE_ETHERNET)
        {
            char smac[18];
            debug = &pAdapterInfo;
            mac_to_string(smac, pAdapterInfo->Address);
            res.push_back(std::string(smac));
        }
        pAdapterInfo = pAdapterInfo->Next;
    }

    return 0;
}

std::string encrypt(const std::string &origin, const std::string &key)
{
    std::string res = "";
    unsigned i = 0;

    for (char sym: origin)
    {
        res.push_back(sym + key[i++]);
        i = i % key.length();
    }

    return res;
}

int main()
{
    int rc;
    std::vector<std::string> macs(0);

    rc = get_mac_addrs(macs);
    if (rc)
    {
        printf("Failed to get system info. err=%d\n", rc);
        return rc;
    }

    std::ofstream fout("license.txt");

    for (const std::string& mac: macs)
        fout << encrypt(mac, KEY) << std::endl;
}
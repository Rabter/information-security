#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <ctime>
#include <cstring>
#include "encryptor.h"
#define KEY_LENGTH 16


int encrypt(const Encryptor &enc, const std::string &f1, const std::string &f2)
{
    std::ifstream fin(f1, std::ios::binary);
    std::ofstream fout(f2, std::ios::binary);

    if (!fin)
    {
        std::cout << "Can not open input file\n";
        return 1;
    }
    if (!fout)
    {
        std::cout << "Can not open output file\n";
        return 1;
    }

    char seq[16];
    unsigned char useq[16];
    do{
        memset(seq, 0, 16);
        fin.read(seq, 16);
        memcpy(useq, seq, 16);
        enc.encrypt(useq);
        memcpy(seq, useq, 16);
        fout.write(seq, 16);
    }while (!fin.eof());

    // Adding meta information about extra zeros amount
    unsigned char extracted[16] = { 0 };
    extracted[0] = fin.gcount();
    enc.encrypt(extracted);
    memcpy(seq, extracted, 16);
    fout.write(seq, 16);

    return 0;
}

int decrypt(const Encryptor &enc, const std::string &f1, const std::string &f2)
{
    std::ifstream fin(f1, std::ios::binary);
    std::ofstream fout(f2, std::ios::binary);

    if (!fin)
    {
        std::cout << "Can not open input file\n";
        return 1;
    }
    if (!fout)
    {
        std::cout << "Can not open output file\n";
        return 1;
    }

    char inseq[16], outseq[16], next[16];
    unsigned char useq[16];
    memset(inseq, 0, 16);
    fin.read(inseq, 16);
    memset(next, 0, 16); // reading next: if inseq is cyphered, next contains meta-inf, otherwise input file is empty
    fin.read(next, 16);
    while (!fin.eof()) // if read is valid, we have to write something to the output file
    {
        memcpy(useq, inseq, 16);
        enc.decrypt(useq);
        memcpy(outseq, useq, 16);

        memcpy(inseq, next, 16);
        memset(next, 0, 16);
        fin.read(next, 16);
        int to_write = 16;
        if (fin.eof()) // If reading next failed, inseq contains info about amount of bytes to write from outseq
        {
            memcpy(useq, inseq, 16);
            enc.decrypt(useq);
            to_write = inseq[0];
        }
        fout.write(outseq, to_write);
    }

    return 0;
}

int main()
{
    char choice = 0;
    std::cout << "Would you like to encrypt or decrypt? (e/d):\n";
    std::cin >> choice;
    choice = std::toupper(choice);
    std::string key = "";
    srand(time(NULL));

    while (choice != 'E' && choice != 'D')
    {
        std::cout << "Input error. Enter 'e' to encrypt or 'd' to decrypt:\n";
        std::cin >> choice;
        choice = std::toupper(choice);
    }

    if (choice == 'E')
    {
        std::cout << "Would you like to create a random key? (Y/n):\n";
        std::cin >> choice;
        choice = std::toupper(choice);

        while (choice != 'Y' && choice != 'N')
        {
            std::cout << "Input error. Enter Y or n.\nWould you like to create a random key? (Y/n):\n";
            std::cin >> choice;
            choice = std::toupper(choice);
        }

        if (choice == 'Y')
        {
            for (int i = 0; i < KEY_LENGTH; ++i)
                key += rand() % 256;
            std::cout << "<key>" << key << "<\\key>\n";
        }
    }

    if (choice == 'D' || choice == 'N')
    {
        while (key.length() != KEY_LENGTH)
        {
            std::cout << "Enter the key:\n";
            std::cin >> key;
            if (key.length() != KEY_LENGTH)
                std::cout << "Bad key. The key must be of " << KEY_LENGTH << " symbols\n";
        }
    }

    unsigned char ukey[KEY_LENGTH];
    memcpy(ukey, key.c_str(), KEY_LENGTH);

    Encryptor enc(ukey);
    std::string f1, f2;

    std::cout << "Enter source file name:\n";
    std::cin >> f1;
    std::cout << "Enter output file name:\n";
    std::cin >> f2;

    int rc;
    if (choice == 'D')
        rc = decrypt(enc, f1, f2);
    else
        rc = encrypt(enc, f1, f2);

    if (!rc)
        std::cout << "Success\n";

    return 0;
}

#ifndef GFBYTE_H
#define GFBYTE_H


class GFByte
{
public:
    inline GFByte(unsigned char byte = 0): byte(byte) {}
    inline GFByte operator + (const GFByte &other) { return this->byte ^ other.byte; }
    GFByte operator * (const GFByte &other);
    inline GFByte operator *= (const GFByte &other) { return *this = *this * other; }
    inline GFByte operator += (const GFByte &other) { return this->byte ^= other.byte; }

    inline unsigned char get() { return byte; }

private:
    unsigned char byte;

    void mul_x();
};

#endif // GFBYTE_H

#include "gfbyte.h"


GFByte GFByte::operator *(const GFByte &other)   // Split 'byte' by bits and sum 'other' multiplied by x to the power of the bit index for every bit that is 1
{
    char i = 0;
    GFByte sum = 0;
    while (i < 8)
    {
        if (byte & 0x01)
        {
            GFByte summand(other);
            for (char j = 0; j < i; ++j)
                summand.mul_x();
            sum += summand;
        }
        byte >>= 1;
        ++i;
    }
    return sum;
}

void GFByte::mul_x()   // Multiplying by x (polynom {02}) is basicly shl with decreasing by 0x011b in overflow
{
    unsigned char highbyte = byte & 0x80;  // If high byte is 1 then shl will cause overflow and we have to subtract 0x011b (if GF(2^8) subtucting and summing is the same and means xor)
    int shifted = byte << 1;
    byte = static_cast<unsigned char>(highbyte? shifted ^ 0x011b : shifted); // Subtracting by xor
}

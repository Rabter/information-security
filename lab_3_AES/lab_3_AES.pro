TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        encryptor.cpp \
        gfbyte.cpp \
        main.cpp

HEADERS += \
    encryptor.h \
    gfbyte.h

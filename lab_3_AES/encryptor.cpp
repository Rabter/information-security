#include <string.h>
#include "encryptor.h"
#include "gfbyte.h"

Encryptor::Encryptor(unsigned char *key)
{
    for (int i = 0; i < ROUND_KEY_LENGTH; ++i)
        rkey[0][i] = key[i];
    key_expansion();
}

void Encryptor::encrypt(unsigned char *seq) const
{
    add_round_key(seq, 0);

    for (unsigned round = 1; round < ROUNDS_AMOUNT; ++round)
    {
        for (unsigned short i = 0; i < ROUND_KEY_LENGTH; ++i)
            seq[i] = sub_byte(seq[i], s_table);
        shift_rows(seq);
        mix_columns(seq, c_table);
        add_round_key(seq, round);
    }

    for (unsigned short i = 0; i < 16; ++i)
        seq[i] = sub_byte(seq[i], s_table);
    shift_rows(seq);
    add_round_key(seq, ROUNDS_AMOUNT);
}

void Encryptor::decrypt(unsigned char *seq) const
{
    add_round_key(seq, ROUNDS_AMOUNT);

    for (unsigned round = ROUNDS_AMOUNT - 1; round > 0; --round)
    {
        inv_shift_rows(seq);
        for (unsigned short i = 0; i < 16; ++i)
            seq[i] = sub_byte(seq[i], inv_s_table);
        add_round_key(seq, round);
        mix_columns(seq, inv_c_table);
    }

    inv_shift_rows(seq);
    for (unsigned short i = 0; i < 16; ++i)
        seq[i] = sub_byte(seq[i], inv_s_table);
    add_round_key(seq, 0);
}

void Encryptor::add_round_key(unsigned char *seq, unsigned short round) const
{
    for (unsigned short i = 0; i < 16; ++i)
        seq[i] ^= rkey[round][i];
}

void Encryptor::rot_word(unsigned char *rkey_word) const  // [a0, a1, a2, a3] -> [a1, a2, a3, a0]
{
    unsigned char buf = rkey_word[0];
    memmove(rkey_word, rkey_word + 1, 3);
    rkey_word[3] = buf;
}

void Encryptor::key_expansion()
{
    for (unsigned short i = 1; i <= ROUNDS_AMOUNT; ++i)
    {
        unsigned char buf[4];
        memcpy(buf, rkey[i - 1] + 12, 4);
        rot_word(buf);
        for (unsigned short j = 0; j < 4; ++j)
            buf[j] = sub_byte(buf[j], s_table);
        buf[0] ^= rcon[i];
        for (unsigned short j = 0; j < 4; ++j)
            rkey[i][j] = rkey[i - 1][j] ^ buf[j];

        for (unsigned short j = 4; j < ROUND_KEY_LENGTH; ++j)
            rkey[i][j] = rkey[i - 1][j] ^ rkey[i][j - 4];
    }
}

unsigned char Encryptor::sub_byte(unsigned char byte, const unsigned char table[16][16]) const
{
    unsigned char row = byte >> 4 & 0x0f;
    unsigned char column = byte & 0x0f;
    return table[row][column];
}

void Encryptor::shift_rows(unsigned char *seq) const
{
    unsigned char buf[4];
    for (unsigned short i = 1; i < 4; ++i)
    {
        memcpy(buf, seq + i * 4, i);
        memmove(seq + 4 * i, seq + i * 5, 4 - i); // seq + i * 5 is an element on main diagonal
        memcpy(seq + 4 + i * 3, buf, i);  // seq + i * 3 + 4 is an element on the right of the side diagonal
    }
}

void Encryptor::inv_shift_rows(unsigned char *seq) const
{
    unsigned char buf[4];
    for (unsigned short i = 1; i < 4; ++i)
    {
        memcpy(buf, seq + 4 + 3 * i, i);
        memmove(seq + 5 * i, seq + 4 * i, 4 - i);
        memcpy(seq + 4 * i, buf, i);
    }
}

void Encryptor::mix_columns(unsigned char *seq, const unsigned char table[4][4]) const
{
    GFByte tmp[ROUND_KEY_LENGTH][ROUND_KEY_LENGTH];
    for (unsigned i = 0; i < 4; ++i)
        for (unsigned j = 0; j < 4; ++j)
            for (unsigned k = 0; k < 4; ++k)
                tmp[i][j] += GFByte(table[i][k]) * GFByte(seq[k * 4 + j]);

    for (unsigned i = 0; i < 4; ++i)
        for (unsigned j = 0; j < 4; ++j)
            seq[i * 4 + j] = tmp[i][j].get();
}

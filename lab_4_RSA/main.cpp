#include <iostream>
#include <fstream>
#include "rsa.h"

int bytes(unsigned long n)
{
    int bits = 0;
    while (n)
    {
        n >>= 1;
        ++bits;
    }
    return bits / 8 + (bits % 8 != 0);
}

int encrypt(const RSAEncryptor &enc, unsigned long n, unsigned long e, const std::string &f1, const std::string &f2)
{
    std::ifstream fin(f1, std::ios::binary);
    std::ofstream fout(f2, std::ios::binary);

    if (!fin)
    {
        std::cout << "Can not open input file\n";
        return 1;
    }
    if (!fout)
    {
        std::cout << "Can not open output file\n";
        return 1;
    }

    char byte;
    int size = bytes(n);
    fin.read(&byte, 1);
    while (!fin.eof())
    {
        unsigned long res = enc.encrypt(static_cast<unsigned char>(byte), n, e);
        char *sym = (char*)&res;
        int shift = sizeof(res) - size;
        for (int i = 0; i < shift; ++i)
            ++sym;
        fout.write(sym, size);
        fin.read(&byte, 1);
    }
    return 0;
}

int decrypt(const RSAEncryptor &enc, unsigned long n, unsigned long d, const std::string &f1, const std::string &f2)
{
    std::ifstream fin(f1, std::ios::binary);
    std::ofstream fout(f2, std::ios::binary);

    if (!fin)
    {
        std::cout << "Can not open input file\n";
        return 1;
    }
    if (!fout)
    {
        std::cout << "Can not open output file\n";
        return 1;
    }

    unsigned long in;
    int size = bytes(n);
    fin.read((char*)&in, size);
    while (!fin.eof())
    {
        int shift = sizeof(in) - size;
        for (int i = 0; i < shift; ++i)
            in >>= 8;
        unsigned char res = enc.decrypt(in, n, d);
        char sym = static_cast<char>(res);
        fout.write(&sym, 1);
        fin.read((char*)&in, size);
    }
    return 0;
}

int main()
{
    RSAEncryptor enc;
    char choice = 0;
    std::cout << "Would you like to encrypt or decrypt? (e/d):\n";
    std::cin >> choice;
    choice = std::toupper(choice);

    while (choice != 'E' && choice != 'D')
    {
        std::cout << "Input error. Enter 'e' to encrypt or 'd' to decrypt:\n";
        std::cin >> choice;
        choice = std::toupper(choice);
    }

    unsigned long n, e, d;
    if (choice == 'E')
    {
        std::cout << "Would you like to create a random key? (Y/n):\n";
        std::cin >> choice;
        choice = std::toupper(choice);

        while (choice != 'Y' && choice != 'N')
        {
            std::cout << "Input error. Enter Y or n.\nWould you like to create a random key? (Y/n):\n";
            std::cin >> choice;
            choice = std::toupper(choice);
        }

        if (choice == 'Y')
        {
            enc.generate_keys(n, e, d);
            std::cout << "public key: n=" << n << " e=" << e << "\n";
            std::cout << "private key: n=" <<  n << " d=" << d << "\n";
        }
        else
        {
            std::cout << "Enter public key (n, e):\n";
            std::cin >> n >> e;
        }
    }
    else
    {
        std::cout << "Enter private key (n, d):\n";
        std::cin >> n >> d;
    }
    std::string fin, fout;
    std::cout << "Enter input file name:\n";
    std::cin >> fin;
    std::cout << "Enter output file name:\n";
    std::cin >> fout;

    int rc;
    if (choice == 'D')
        rc = decrypt(enc, n, d, fin, fout);
    else
        rc = encrypt(enc, n, e, fin, fout);

    if (!rc)
        std::cout << "Success\n";
    return 0;
}

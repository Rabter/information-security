#ifndef RSA_H
#define RSA_H

class RSAEncryptor
{
public:
    RSAEncryptor();

    void generate_keys(unsigned long &n, unsigned long &e, unsigned long &d) const;
    unsigned long encrypt(unsigned char original, unsigned long key_n, unsigned long key_e) const;
    unsigned char decrypt(unsigned long enc, unsigned long key_n, unsigned long key_d) const;

private:
    unsigned long power_by(unsigned long num, unsigned long power, unsigned long divider) const;
    unsigned long gdc(unsigned long a, unsigned long b) const;
    unsigned long ext_gdc(unsigned long a, unsigned long b, unsigned long &x, unsigned long &y) const;
    unsigned long rand_prob_simple() const;
    bool test_prob_simple(unsigned long num) const;
    unsigned long n, e, d;
};


#endif // RSA_H

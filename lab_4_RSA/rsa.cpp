#include <random>
#include <ctime>
#include "rsa.h"
#define K 16

RSAEncryptor::RSAEncryptor()
{
    srand(time(nullptr));
}

unsigned long RSAEncryptor::power_by(unsigned long num, unsigned long power, unsigned long divider) const
{    
    unsigned long long buf = 1, longnum = num;
    while (power)
    {
        if (power % 2)
            buf = buf * longnum % divider;
        longnum = longnum * longnum %  divider;
        power >>= 1;
    }
    return static_cast<unsigned long>(buf);
}

unsigned long RSAEncryptor::gdc(unsigned long a, unsigned long b) const
{
    unsigned long c;
    while (b)
    {
        c = a % b;
        a = b;
        b = c;
    }
    return a;
}

unsigned long RSAEncryptor::ext_gdc(unsigned long a, unsigned long b, unsigned long &x, unsigned long &y) const
{
    if (a == 0)
    {
        x = 0;
        y = 1;
        return b;
    }
    unsigned long x1, y1;
    unsigned long d = ext_gdc(b%a, a, x1, y1);
    x = y1 - (b / a) * x1;
    y = x1;
    return d;
}

unsigned long RSAEncryptor::rand_prob_simple() const
{
    unsigned long num;
    do{
        num = rand() + 2;
    }while (!test_prob_simple(num));
    return num;
}

bool RSAEncryptor::test_prob_simple(unsigned long num) const
{
    if (num % 2)
    {
        int s = 1;
        unsigned long t = num >> 1;
        while (!(t % 2))
        {
            t >>= 1;
            ++s;
        }
        char passed = 0;
        for (int k = K; k >= 0 && !passed; --k)
        {
            unsigned long witness = (rand() + 2) % (num - 2);
            unsigned long x = power_by(witness, t, num);
            if (x == 1 || x == num - 1)
                passed = 1;
            for (int i = 1; i < s && !passed; ++i)
            {
                x = power_by(x, 2, num);
                if (x == 1)
                    passed = -1;
                else if (x == num - 1)
                    passed = 1;
            }
            if (passed != -1)
                passed -= 1;
        }
        return passed != -1;
    }
    return false;
}

unsigned long RSAEncryptor::encrypt(unsigned char original, unsigned long key_n, unsigned long key_e) const
{
    return power_by(original, key_e, key_n);
}

unsigned char RSAEncryptor::decrypt(unsigned long enc, unsigned long key_n, unsigned long key_d) const
{
    return power_by(enc, key_d, key_n);
}

void RSAEncryptor::generate_keys(unsigned long &n, unsigned long &e, unsigned long &d) const
{
    int p = rand_prob_simple(), q = rand_prob_simple();
    n = p * q;
    unsigned long f = (p - 1) * (q - 1);
    do{
        e = rand() % f;
    }while (gdc(e, f) != 1);

    unsigned long k;
    ext_gdc(e, f, d, k);
}

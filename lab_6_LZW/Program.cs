﻿using System;

namespace lab_6_LZW
{
    class Program
    {
        static void Main(string[] args)
        {
            LZW archiver = new LZW();
            string choice;
            do
            {
                Console.WriteLine("Select an option:\n1. Compress\n2. Decompress");
                choice = Console.ReadLine();
            } while (choice != "1" && choice != "2");
            if (choice == "1")
            {
                Console.WriteLine("Enter file name:");
                string file = Console.ReadLine();
                Console.WriteLine("Enter archive file name:");
                string archive = Console.ReadLine();
                archiver.compress(file, archive);
                Console.WriteLine("The file has been sucsessfully compressed");
            }
            else
            {
                Console.WriteLine("Enter archive file name:");
                string archive = Console.ReadLine();
                Console.WriteLine("Enter result file name:");
                string file = Console.ReadLine();
                archiver.decompress(archive, file);
                Console.WriteLine("The file has been sucsessfully decompressed");
            }
            Console.Read();
        }
    }
}

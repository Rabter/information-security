﻿using System.Collections.Generic;
using System.IO;

namespace lab_6_LZW
{
    class LZW
    {
        const ushort maxDictionaryLength = 65000;
        public int compress(string fileName, string archiveName)
        {
            int ret = 0;
            List<string> dict = new List<string>();
            for (int sym = 0; sym < 256; sym++)
                dict.Add(((char)sym).ToString());

            //long processed = 0;
            using (FileStream input = new FileStream(fileName, FileMode.Open))
            {
                using (BinaryWriter output = new BinaryWriter(new FileStream(archiveName, FileMode.Create)))
                {
                    string sym = "";
                    int nextByte = input.ReadByte();
                    while (nextByte != -1)
                    {
                        string extented = sym + (char)nextByte;
                        int index, code = -1;
                        while ((index = dict.FindIndex(x => x == extented)) != -1)
                        {
                            sym = extented;
                            nextByte = input.ReadByte();
                            extented += (char)nextByte;
                            code = index;
                        }

                        if (dict.Count < maxDictionaryLength)
                            dict.Add(extented);
                        else
                            ret = 1;
                        output.Write((ushort)code);
                        sym = "";
                        //processed++;
                    }
                }
            }
            return ret;
        }

        public void decompress(string archiveName, string fileName)
        {
            List<string> dict = new List<string>();
            for (int sym = 0; sym < 256; sym++)
                dict.Add(((char)sym).ToString());

            using (FileStream inStream = new FileStream(archiveName, FileMode.Open))
            using (BinaryReader input = new BinaryReader(inStream))
            {
                if (inStream.Position < inStream.Length)
                {
                    using (FileStream outStream = new FileStream(fileName, FileMode.Create))
                    {
                        ushort next;
                        string sym = dict[input.ReadUInt16()], nextsym;
                        bool go = true;
                        do
                        {
                            foreach (char c in sym)
                                outStream.WriteByte((byte)c);
                            if (inStream.Position < inStream.Length)
                            {
                                next = input.ReadUInt16();
                                if (next == dict.Count)
                                    nextsym = sym + sym[0];
                                else
                                    nextsym = dict[next];
                                dict.Add(sym + nextsym[0]);
                                sym = nextsym;
                            }
                            else
                                go = false;
                        } while (go);
                    }
                }
            }
        }
    }
}
